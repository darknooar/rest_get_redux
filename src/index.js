import  React, { Component } 			from 'react';
import  ReactDOM 						from 'react-dom';
import  {Provider, connect} 			from 'react-redux';
import  {createStore, applyMiddleware}  from 'redux';
import  thunk 							from 'redux-thunk';
import  './style.css'

import  registerServiceWorker 			from './registerServiceWorker';


function fetchPostsRequest(){
	return {
		type: "FETCH_REQUEST"
	}
}

function fetchPostsSuccess(payload) {
	return {
		type: "FETCH_SUCCESS",
		payload
	}
}

function fetchPostsError() {
	return {
		type: "FETCH_ERROR"
	}
}

const initialState = {

}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case "FETCH_REQUEST":
			return state;
		case "FETCH_SUCCESS": 
			return {...state, posts: action.payload};
		default:
			return state;
	}
} 

function fetchPostsWithRedux() {
	return (dispatch) => {
		dispatch(fetchPostsRequest());

		return fetchPosts()
			.then(([response, json]) => {
					dispatch(fetchPostsSuccess(json))
			})
	}
}

function fetchPosts() {
	//const URL = "http://www.mocky.io/v2/5995fab61100008906cc43ac";
	const URL = "http://jsonplaceholder.typicode.com/posts";
	return fetch(URL, { method: 'GET'})
		.then( response => Promise.all(
			[response, response.json()])
		);
}

class App extends Component {

	componentDidMount(){
  		this.props.fetchPostsWithRedux()
	}

	render(){
		return (
			<table>
				<th> id 	</th>
				<th> title  </th>
				<th> body   </th>
				{
					this.props.posts && 
					this.props.posts.map((item) =>{
						return(
							<tr key={item.id}>

								<td>{item.id}</td>
								<td>{item.title}</td>
								<td>{item.body}</td>

							</tr>
						)
					})
				}
			</table>
		)
	}
}


function mapStateToProps(state){
	return {
		posts: state.posts
	}
}


let Container = connect(mapStateToProps, {fetchPostsWithRedux})(App);

const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

const lol = 

<Provider store={store}>
    <Container/>
</Provider>

ReactDOM.render(
    lol,
    document.getElementById('root')
);



registerServiceWorker();
